[![](https://img.shields.io/badge/微信号-代码界的小白-orange.svg)](https://s3.bmp.ovh/imgs/2022/01/8e047c64dc244b45.jpg)
[![](https://img.shields.io/badge/公众号-代码界的小白-success.svg)](https://mp.weixin.qq.com/s?__biz=MzU5NTkxODM5Ng==&mid=2247485232&idx=1&sn=c3068f21cb118a680309b191fb5be509&chksm=fe6bec63c91c6575153b7274993df54ecc11ff241dff6ad9089bbca320b5ce8932a32a2d1aba&token=937009468&lang=zh_CN#rd)
[![](https://img.shields.io/badge/知乎-代码界的小白-blue.svg)](https://www.zhihu.com/people/dai-ma-jie-de-xiao-bai)
[![](https://img.shields.io/badge/力扣-代码界的小白-important.svg)](https://leetcode-cn.com/u/zzpig/)
[![](https://img.shields.io/badge/牛客-代码界的小白-ff69b4.svg)](https://www.nowcoder.com/profile/3639882/myDiscussPost)






# 1、[Java学习路线推荐](https://mp.weixin.qq.com/s?__biz=MzU5NTkxODM5Ng==&mid=2247485232&idx=1&sn=c3068f21cb118a680309b191fb5be509&chksm=fe6bec63c91c6575153b7274993df54ecc11ff241dff6ad9089bbca320b5ce8932a32a2d1aba&token=937009468&lang=zh_CN#rd)

# 2、[Java经典电子版书](https://mp.weixin.qq.com/s?__biz=MzU5NTkxODM5Ng==&mid=2247485327&idx=1&sn=fdd00ad839f768d78b9c0db0a1d1080a&chksm=fe6becdcc91c65ca49b2129b46781f3e2ef4ae9626379a94ac604493f2b0eb740bd221375b0e&token=937009468&lang=zh_CN#rd)


           

# 3、Java面试必知必会

## 介绍
本仓库主要讲解Java在面试中的高频考点，分别包含Java基础、Java集合、Java多线程与并发编程、Java虚拟机、数据库、计算机基础、框架和中间件等。

## 3.1 Java基础知识

    1.Java基础知识在面试中的高频考点
    2.Java集合在面试中的高频考点
    3.Java多线程与并发编程在面试中的高频考点
    4.Java虚拟机在面试中的高频考点


## 3.2 数据库知识
### 3.2.1 MySQL高频面试问题

    1. MySQL索引高频面试问题
    2. MySQL锁的高频面试问题
    3. MySQL事务的高频面试问题

### 3.2.2 Redis的高频面试问题

## 3.3 设计模式相关


## 3.4 中间件等相关
    1.消息中间件高频面试问题
    2.Netty高频面试问题


## 3.5 Spring相关框架

    1.Spring高频面试问题
    2.SpringBoot的高频面试问题

## 3.6 计算机基础知识
    1.计算机网络高频面试问题
    2.操作系统高频面试问题




## 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
